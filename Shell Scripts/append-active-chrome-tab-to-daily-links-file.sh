#!/bin/bash
DATE=$(date +"%Y-%m-%d")
TIME=$(date +"%r")
TITLEVAR=$(date '+%B %e, %Y')

if [ -f "/Users/christophersu/Dropbox-Gamma/Dropbox/Personal/Links/$DATE.md" ]
then
	echo -e "* \`$TIME:\` __$2__  \n<$1>" >> /Users/christophersu/Dropbox-Gamma/Dropbox/Personal/Links/$DATE.md
else
	echo -e "# $TITLEVAR" >> /Users/christophersu/Dropbox-Gamma/Dropbox/Personal/Links/$DATE.md
	echo -e "* \`$TIME:\` __$2__  \n<$1>" >> /Users/christophersu/Dropbox-Gamma/Dropbox/Personal/Links/$DATE.md
fi