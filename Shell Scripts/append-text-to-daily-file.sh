#!/bin/bash
DATE=$(date +"%Y-%m-%d")
TIME=$(date +"%r")
TITLEVAR=$(date '+%B %e, %Y')

if [ -f "/Users/christophersu/Dropbox-Gamma/Dropbox/Dailies/$DATE.md" ]
then
	echo -e "\n## $TIME\n$1" >> /Users/christophersu/Dropbox-Gamma/Dropbox/Dailies/$DATE.md
else
	echo -e "# $TITLEVAR" >> /Users/christophersu/Dropbox-Gamma/Dropbox/Dailies/$DATE.md
	echo -e "[[_TOC_]]" >> /Users/christophersu/Dropbox-Gamma/Dropbox/Dailies/$DATE.md
	echo -e "\n## $TIME\n$1" >> /Users/christophersu/Dropbox-Gamma/Dropbox/Dailies/$DATE.md
fi